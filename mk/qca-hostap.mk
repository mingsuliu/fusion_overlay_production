spf:= 9.0 10.0 11.0

define qca-hostap_overlay_patch

	if [ ! -d $(TOPDIR)/qca/src/$(PKG_NAME)/qca-patches ]; then \
		mkdir $(TOPDIR)/qca/src/$(PKG_NAME)/qca-patches; \
	fi

	$(foreach rev,$(spf), \
		$(if $(findstring y,$(CONFIG_PACKAGE_fuqca-hostap_$(rev))), \
			if [ -d $(TOPDIR)/nucleus/src/fu$(PKG_NAME)/SPF$(rev)/patches/ ]; then \
				$(CP) $(TOPDIR)/nucleus/src/fu$(PKG_NAME)/SPF$(rev)/patches/* $(TOPDIR)/qca/src/$(PKG_NAME)/qca-patches/; \
			fi \
		) \
	)

	$(foreach rev,$(spf), \
		$(if $(findstring y,$(CONFIG_PACKAGE_fuqca-hostap_$(rev))), \
			if [ -d $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/SPF$(rev)/patches/ ]; then \
				$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/SPF$(rev)/patches/* $(TOPDIR)/qca/src/$(PKG_NAME)/qca-patches/; \
			fi \
		) \
	)
endef

define qca-hostap_config_patch
	$(foreach rev,$(spf), \
		$(if $(findstring y,$(CONFIG_PACKAGE_fuqca-hostap_$(rev))), \
			if [ -e $(TOPDIR)/nucleus/src/fu$(PKG_NAME)/SPF$(rev)/hostapd-default.config ]; then \
				$(CP) $(TOPDIR)/nucleus/src/fu$(PKG_NAME)/SPF$(rev)/hostapd-default.config $(PKG_BUILD_DIR)/hostapd/.config; \
			fi \
		) \
	)

	$(foreach rev,$(spf), \
		$(if $(findstring y,$(CONFIG_PACKAGE_fuqca-hostap_$(rev))), \
			if [ -e $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/SPF$(rev)/hostapd-default.config ]; then \
				$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/SPF$(rev)/hostapd-default.config $(PKG_BUILD_DIR)/hostapd/.config; \
			fi \
		) \
	)
endef

define qca-hostap_overlay_install

	if [ -d $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME) ]; then \
		$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/files/qca-hostapd.init $(1)/etc/init.d/qca-hostapd; \
		$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/files/hostapd.sh $(1)/lib/wifi/hostapd.sh; \
	fi

endef

Package/qca-hostap/install += $(qca-hostap_overlay_install)

Hooks/Prepare/Pre += qca-hostap_overlay_patch
Hooks/Configure/Post += qca-hostap_config_patch 
