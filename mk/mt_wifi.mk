softap:= 6.0
Hooks/Prepare/Pre += zyxel_uMAC_install_patches
define zyxel_uMAC_install_patches

	if [ ! -d patches ]; then \
		mkdir patches; \
	fi

	$(foreach rev,$(softap), \
		$(if $(findstring y,$(CONFIG_PACKAGE_zyUMAC_$(rev))), \
			if [ -d $(TOPDIR)/nucleus/src/zyUMAC/MTK$(rev)/patches/ ]; then \
				$(CP) $(TOPDIR)/nucleus/src/zyUMAC/MTK$(rev)/patches/* patches/; \
			fi \
		) \
	)

endef

Hooks/Prepare/Post += zyxel_uMAC_src_install
define zyxel_uMAC_src_install
	[ ! -d $(TOPDIR)/nucleus/src/zyUMAC ] || \
		mkdir $(PKG_BUILD_DIR)/zyUMAC
		$(CP) $(TOPDIR)/nucleus/src/zyUMAC/* $(PKG_BUILD_DIR)/zyUMAC
endef

define zyMAC_dev_post_install
	$(CP) $(PKG_BUILD_DIR)/zyUMAC/inc/*.h $(STAGING_DIR)/usr/include/
endef
Build/InstallDev+=$(zyMAC_dev_post_install)
