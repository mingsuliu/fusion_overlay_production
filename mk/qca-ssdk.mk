spf:= 11.0

Hooks/Prepare/Pre += qca-ssdk_install_patches
define qca-ssdk_install_patches

	if [ ! -d patches ]; then \
		mkdir patches; \
	fi

	$(foreach rev,$(spf), \
		$(if $(findstring y,$(CONFIG_PACKAGE_fuMAC_$(rev))), \
			if [ -d $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/SPF$(rev)/patches/ ]; then \
				$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/SPF$(rev)/patches/* patches/; \
			fi \
		) \
	)
endef

