define fusion_qcahk_post_install
	;\
	[ ! -d $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME) ] || \
		$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/files/* $(1)/

	[ ! -d $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/$(CONFIG_FUSION_MODEL_NAME) ] || \
		$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/$(CONFIG_FUSION_MODEL_NAME)/* $(1)/

endef
Package/$(PKG_NAME)-hw1-10.4-asic/install += $(fusion_qcahk_post_install)
